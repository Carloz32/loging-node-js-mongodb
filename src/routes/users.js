const router = require('express').Router();
const User = require('../models/User');
const passport = require('passport');
//Inicion de sesion
router.get('/users/signin', (req, res) => {
    res.render('users/signin');
});
//Registro de usuaro
router.get('/users/signup', (req, res) => {
    res.render('users/signup');
});
//Autenticacion de usuario 
router.post('/users/signin', passport.authenticate('local', {
    successRedirect: '/tasks',
    failureRedirect: '/users/signin',
    failureFlash: true
}));
router.post('/users/signup', async (req, res) => {
    const { nombre, apellido, edad, genero, email, password, confirmPassword } = req.body;
    const errors = []
    const emailUser = await User.findOne({ email: email });
//Validacion de contraseña
    if (password.length <= 0) {
        errors.push({ text: 'Por favor ingrese una contraseña' });
    }
    if (password != confirmPassword) {
        errors.push({ text: 'Las contraseñas no coinciden' });
    }
    if (password.length < 8) {
        errors.push({ text: 'La contraseña debe ser al menos 8 caracteres' });
    }
    if (emailUser) {
        errors.push({ text: 'El correo electronico ya esta en uso' });

    }
    if (errors.length > 0) {
        res.render('users/signup', { errors, nombre, apellido, edad, genero, email, password, confirmPassword });
    }

    else {
        //Registro de nuevo usuario
        const newUser = new User({ nombre, apellido, edad, genero, email, password });
        newUser.password = await newUser.encryptPassword(password);
        await newUser.save();
        req.flash('success_msg', 'Ususario registrado correctamente');
        res.redirect('/users/signin');
    }
});
//Cerrar sesion
router.get('/users/logout',(req,res)=>{
    req.logOut();
    res.redirect('/');
});

module.exports = router;
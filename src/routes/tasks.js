const router = require('express').Router();

const Task = require('../models/Task');
const {isAuthenticated}= require('../helpers/out');

router.get('/tasks/add', isAuthenticated,(req, res) => {
    res.render('tasks/new-task');
});
//Validación de campos para el regiistro
router.post('/tasks/new-task',isAuthenticated, async (req, res) => {
    const { nombre, prioridad, fecha } = req.body;
    const errors = [];
    if (!nombre) {
        errors.push({ text: 'Por favor inserte un nombre' })
    }
    if ("Selecione la prioridad" == prioridad) {
        errors.push({ text: 'Por favor ingrese una prioridad valida' })
    }
    if (!fecha) {
        errors.push({ text: 'Por favor ingrese una fecha valida' })
    }
    if (errors.length > 0) {
        res.render('tasks/new-task', {
            errors,
            nombre,
            prioridad,
            fecha
        })
    } else {

        const newTask = new Task({ nombre, prioridad, fecha })
        newTask.user=req.user.id;
        await newTask.save();
        req.flash('success_msg','Tarea agregada')

        res.redirect('/tasks');
    }

});
//Busqueda de tareas para cada usuario
router.get('/tasks',isAuthenticated, async(req, res) => {
    const tasks = await Task.find({user:req.user.id});
    res.render('tasks/all-tasks',{tasks});
});

//Busqueda de datos de una tarea
router.get('/tasks/edit/:id',isAuthenticated, async(req,res)=>{
    const task = await Task.findById(req.params.id)
    res.render('tasks/edit-task',{task});
});
//Actualizacion de tarea
router.put('/tasks/edit-task/:id',isAuthenticated, async(req,res)=>{
    const { nombre, prioridad, fecha } = req.body;
    await  Task.findByIdAndUpdate(req.params.id,{nombre, prioridad, fecha});
    req.flash('success_msg','Tarea actualizada');
    res.redirect('/tasks');
});

//Eliminacion de tarea
router.delete('/tasks/delete/:id',isAuthenticated,async(req,res)=>{
    await Task.findByIdAndRemove(req.params.id);
    req.flash('success_msg','Tarea eliminada');
    res.redirect('/tasks');
});
module.exports = router;
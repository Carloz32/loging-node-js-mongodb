const mongoose= require ('mongoose');
const {Schema}=mongoose;

const TaskSchema =new Schema({
    nombre:{type: String,required:true},
    prioridad:{type: String,required:true},
    fecha:{type: String,required:true},
    user:{type: String}
});

module.exports=mongoose.model('Task',TaskSchema)